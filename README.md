# Mining Intuitive Referring Expressions in Knowledge Bases

## Compiling the code

### Software Requirements

* Java >= 8
* Maven 

### Compilation

Clone the repository

$ git clone http://gitlab.inria.fr/lgalarra/remi.git

Using a command line interface, go to the directory where you cloned the git repository and run:

$ mvn install:install-file -Dfile=lib/hdt-api-2.0-SNAPSHOT.jar -DgroupId=org.rdfhdt -DartifactId="hdt-api" -Dversion=2.0-SNAPSHOT -Dpackaging=jar <br />
$ mvn install:install-file -Dfile=lib/hdt-java-core-2.0-SNAPSHOT.jar -DgroupId=org.rdfhdt -DartifactId="hdt-java-core" -Dversion=2.0-SNAPSHOT -Dpackaging=jar <br />
$ mvn install:install-file -Dfile=lib/hdt-jena-2.0-SNAPSHOT.jar -DgroupId=org.rdfhdt -DartifactId="hdt-jena" -Dversion=2.0-SNAPSHOT -Dpackaging=jar <br />

$ mvn package -DskipTests

This will produce three target files:

* target/remi.jar (REMI's standalone program)
* target/remi-experimental.jar (REMI's software to generate the data for the runtime experiments)
* target/reamie.jar (AMIE's experimental adaptation for the runtime experiments)

You can also import the project using Eclipse and its plugin for Maven. REMI's main class is `fr.inria.res.REMiner`. The class to measure REMI's runtime is `fr.res.experimental.REMinerMeasureRuntime`, whereas the class to measure AMIE's runtime is `fr.res.experimental.REAMIEMiner`.

## Using the binaries

### REMI's standalone software

This software allows testing REMI on a single set of entities. 

Run in a command line:

$ java -jar remi.jar -kb <KB> -s disk -exh [-ed] [-ee|-eef] [-nb &lt;n-cores&gt;] e1 [e2, ... en]

* &lt;KB&gt; is the target KB. At the moment we support 'dbpedia' and 'wikidata'. For example if &lt;KB&gt;=dbpedia, the program searches for a folder name 'src/data/dbpedia' colocated with the .jar. The folder must contain an HDT file dbpedia.hdt as well as some other files with several metadata.
* -exh means exhaustive search (obligatory flag)
* &lt;n-cores&gt; is the number of cores to use. By default REMI uses 1. If the word 'all' is given, REMI uses all available cores. This configuration is what we call P-REMI. 
* -ed (Optional) means "enable debugging". If present it leads to a very verbose output.
* -eef or -ee (only one of them at a time is accepted). If absent REMI mines REs according to the state-of-the-art language bias. -eef means instructs the system to use the  full REMI's language bias, i.e., all the types of subgraph expressions described in Figure 1 in the paper. -ee considers only the subgraph expressions consisting of up to 2 atoms in Figure 1. 
* e_i : the ith target entity. REMI requires full IRIs, e.g., http://dbpedia.org/resource/Salzburg

### Experimental software

To measure REMI's runtime on multiple instances, use remi-experimental.jar as follows:

$ java -jar remi-experimental.jar &lt;KB-folder&gt; &lt;KB-file&gt; &lt;entities-file&gt; [timeout] [mode]

* &lt;KB-folder&gt; is the directory containing the KB in HDT format as well as the metadata files. (read section Data under "Data and Experimental Results")
* &lt;KB-file&gt; the name of the KB file (.hdt)
* &lt;entities-file&gt; A text file where each line contains a set of entities (IRIs) separated by space
* timeout in minutes indicates the time before the REMI is interrupted if it has not found a solution (default value = 30).   
* mode It can be sremi-noext (REMI on the state-of-the-art language bias), sremi-ext (REMI on the extended language bias), premi-noext (P-REMI on the state-of-the-art language bias) and premi-ext (P-REMI on the extended language bias). You can provide a concatenation of multiple values e.g., "sremi-noext,premi-ext". If no value is provided for this argument, the program runs all modes. 

To measure AMIE's runtime on multiple instances, use reamie.jar as follows:

$ java -jar reamie.jar &lt;KB&gt; &lt;entities-file&gt; &lt;coefficients&gt; [timeout] [enable-remi-language-bias]

* &lt;KB-file&gt; is the file with the KB in TSV format (read section Data under "Data and Experimental Results")
* &lt;entities-file&gt; A text file where each line contains a set of entities (IRIs) separated by space. The IRIS must appear as in the TSV file. If the TSV file uses brackets, then provide IRIs such as &lt;http://dbpedia.org/resource/Salzburg&gt; 
* &lt;coefficients&gt; The coefficients file associated to the KB. (read section Data under "Data and Experimental Results")
* timeout in minutes indicates the time before the AMIE is interrupted if it has not found a solution (default value = 30)
* enable-remi-language-bias if 'true', the program mines REs according to REMI's extended language bias, otherwise it mines REs according to the state-of-the-art language. (default value 'false').

## Data and Experimental Results

### Data

The datasets in TSV and HDT format are located src/data/dbpedia and src/data/wikidata. In both cases you will see at least the following files:

1. &lt;KB&gt;.hdt: the KB in HDT format
2. &lt;KB&gt;.hdt.index.v1-1: index created automatically by the HDT Jena bridge to speed-up queries
3. ranking_relations/coefficients: the coefficients that allow for the association between complexity and frequency for predicates (Section 3.5, Complexity function in the paper)
4. topentities: the entities that are considered prominent (under your favorite criterion, we used frequency in the KB) by REMI. These are used to prune the search space as explained in Section 3.5, Algorithms, in the paper. 
5. experimental/ : This folder contains the input files with the entities used for the runtime experiments.
6. dbpedia_plus_inv_facts.tsv (for DBpedia) and wikidata-simple-statements.compressed.clean.ids-labeled.plus-interesting-literals.plus-inv-facts.tsv (for Wikidata) are the corresponding TSV versions of the HDT files. These files are not available in this repository due to their large size. Please contact luis.galarraga@inria.fr if you want a copy.

Note: For the Wikidata quality experiments, we manually removed some predicates that are useless for descriptions. We therefore produced a new file called wikidata-clean.hdt from wikidata-simple-statements.compressed.clean.ids-labeled.plus-interesting-literals.plus-inv-facts.cleaned-again.nt (contact luis.galarraga@inria.fr if you want a copy of them)

If you want to integrate a new KB into REMI, you need to create a directory with at least files 1 to 4.

### Runtime Evaluation

The results of the runtime evaluation can be found under src/data/experiments/runtime/remi-runtime-evaluation.ods

### Qualitative Evaluation

The forms used to conduct the experimental evaluation are available online.

* Complexity function evaluation: https://goo.gl/forms/dwRUi4erTZNP1XoF3 (English), https://goo.gl/forms/AoSzCW1YKJfPjYaf1 (French)
* REMI's output evaluation: https://goo.gl/forms/SS9O0h6F1FeCfaD92 (English),  https://goo.gl/forms/NpSHmpfNmfOUK92s2 (French)
* Data quality evaluation: https://goo.gl/forms/uUjZhHryEuvbbXaK2


The versions in pdf are available under src/data/experiments/qualitative

The experimental data produced by the qualitative evaluation is available under src/data/experiments/qualitative/remi-complexity-function-and-output-evaluation.ods and src/data/experiments/qualitative/remi-data-quality-evaluation.ods