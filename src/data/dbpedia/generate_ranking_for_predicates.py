#!/usr/bin/python3

import csv
import sys
import operator
import re
import random

from add_inverse_facts import parseLine

def getRelationsAndEntitiesByDegree(fileName):
    ## Read the dbpedia file directly
    with open(fileName, 'r') as fdata :
        lineNumber = 0
        predicatesDict = {}
        predicates2EntitiesDict = {}
        entitiesDict = {}

        for line in fdata :
            if line.startswith('#') :
                continue
            
            lineNumber += 1
            subject, predicate, object = parseLine(line, lineNumber)
            
            countObject = False
            
            isTypePredicate = (predicate == '<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>' or predicate == '<http://wikidata.org/property/instance_of_P31>')
            if isTypePredicate :
                if object not in entitiesDict :
                    entitiesDict[object] = 0
            
                entitiesDict[object] += 1
            else :                        
                if subject not in entitiesDict :
                    entitiesDict[subject] = 0
            
                entitiesDict[subject] += 1
            
            if predicate not in predicatesDict :
                predicatesDict[predicate] = 0
                predicates2EntitiesDict[predicate] = {}
                
            predicatesDict[predicate] += 1
            
            if object not in predicates2EntitiesDict[predicate] :
                predicates2EntitiesDict[predicate][object] = 0      
        
        for predicate in predicates2EntitiesDict :
            for entity in predicates2EntitiesDict[predicate] :
                if entity in entitiesDict :
                    predicates2EntitiesDict[predicate][entity] = entitiesDict[entity]
            
            
    return predicatesDict, predicates2EntitiesDict, entitiesDict

if __name__ == '__main__' :
    ## Get the relevance of entities (number of facts)
    predicatesDict, predicates2EntitiesDict, entitiesDict = getRelationsAndEntitiesByDegree(sys.argv[1])      
    # Now we have to do a second pass on the data to invert the facts  
    sortedPredicates = [x[0] for x in sorted(predicatesDict.items(), key=operator.itemgetter(1), reverse=True)]
                       
            
    with open('cached_relation_queries', 'w') as ftop :
        for predicate in sortedPredicates :
            ftop.write(predicate.replace('<', '').replace('>', '') + " " + str(predicatesDict[predicate]))
            ftop.write('\n')
            
    with open('cached_relation_object_counts', 'w') as ftop :
        for predicate in sortedPredicates :
            ftop.write(predicate.replace('<', '').replace('>', '') + " " + str(len(predicates2EntitiesDict[predicate])))
            ftop.write('\n')
             
    for predicate in sortedPredicates :
        if predicatesDict[predicate] < 50 :
            break
        
        predName = predicate.replace('<', '').replace('>', '').replace('/', '')
        
        with open('ranking_relations/ranking_' + predName, 'w') as fpred :  
            fpred.write(predicate + '\n')
            sortedEntities = [x[0] for x in sorted(predicates2EntitiesDict[predicate].items(), key=operator.itemgetter(1), reverse=True)]           
            for entity in sortedEntities :
                fpred.write(entity.replace('<', '').replace('>', '') + " " + str(predicates2EntitiesDict[predicate][entity]))
                fpred.write('\n') 
