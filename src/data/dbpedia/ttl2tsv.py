#!/usr/bin/python3

import csv
import sys
import operator
import re
import random

from add_inverse_facts import parseLine

if __name__ == '__main__' :
    with open('../wikidata/wikidata-simple-statements.compressed.clean.ids-labeled.plus-interesting-literals.plus-inv-facts.nt', 'r') as fdata :
        lineNumber = 1
        for line in fdata :
            if line.startswith('#') :
                continue
            
            subject, predicate, object = parseLine(line, lineNumber) 
            print(subject, '\t', predicate, '\t', object)
            lineNumber += 1
