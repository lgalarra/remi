#!/usr/bin/python3

import sys

with open('wikidata-simple-statements.compressed.clean.ids-labeled.plus-interesting-literals.tsv') as f :
	for line in f :
		parts = line.strip('\n').split('\t')
		if len(parts) < 3 :
			continue
		
		subject = parts[0].replace('<', '<http://wikidata.org/resource/')
		predicate = parts[1].replace('<', '<http://wikidata.org/property/')
		obj = parts[2]
		if obj.startswith('<') :
			obj = obj.replace('<', '<http://wikidata.org/resource/')
			
		print(subject, predicate, obj, '.')
