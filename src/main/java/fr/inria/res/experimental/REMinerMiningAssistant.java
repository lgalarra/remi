package fr.inria.res.experimental;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import amie.data.KB;
import amie.mining.assistant.experimental.InstantiatedHeadMiningAssistant;
import amie.rules.Rule;
import javatools.datatypes.ByteString;
import javatools.datatypes.IntHashMap;

public class REMinerMiningAssistant extends InstantiatedHeadMiningAssistant {

	public REMinerMiningAssistant(KB dataSource) {
		super(dataSource);
	}

	@Override
	public void getClosingAtoms(Rule rule, double minSupportThreshold, Collection<Rule> output) {
		
	}
	
	protected void getDanglingAtoms(Rule query, ByteString[] edge, double minSupportThreshold, 
			List<ByteString> joinVariables, Collection<Rule> output) {
		int nPatterns = query.getLength();
		
		for (ByteString joinVariable: joinVariables){
			int joinPosition = 0;
			ByteString[] newEdge = edge.clone();
			
			newEdge[joinPosition] = joinVariable;
			query.getTriples().add(newEdge);
			IntHashMap<ByteString> promisingRelations = null;
			Rule rewrittenQuery = null;
			if (this.enableQueryRewriting) {
				rewrittenQuery = rewriteProjectionQuery(query, nPatterns, joinPosition == 0 ? 0 : 2);	
			}
			
			if(rewrittenQuery == null){
				long t1 = System.currentTimeMillis();
				promisingRelations = this.kb.countProjectionBindings(query.getHead(), query.getAntecedent(), newEdge[1]);
				long t2 = System.currentTimeMillis();
				if((t2 - t1) > 20000 && this.verbose) {
					System.err.println("countProjectionBindings var=" + newEdge[1] + " "  + query + " has taken " + (t2 - t1) + " ms");
				}
			}else{
				long t1 = System.currentTimeMillis();
				promisingRelations = this.kb.countProjectionBindings(rewrittenQuery.getHead(), rewrittenQuery.getAntecedent(), newEdge[1]);
				long t2 = System.currentTimeMillis();
				if((t2 - t1) > 20000 && this.verbose)
				System.err.println("countProjectionBindings on rewritten query var=" + newEdge[1] + " "  + rewrittenQuery + " has taken " + (t2 - t1) + " ms");						
			}
			
			query.getTriples().remove(nPatterns);					
			int danglingPosition = (joinPosition == 0 ? 2 : 0);
			boolean boundHead = !KB.isVariable(query.getTriples().get(0)[danglingPosition]);
			List<ByteString> listOfPromisingRelations = promisingRelations.decreasingKeys();				
			// The relations are sorted by support, therefore we can stop once we have reached
			// the minimum support.
			for(ByteString relation: listOfPromisingRelations){
				int cardinality = promisingRelations.get(relation);
				
				if (cardinality < minSupportThreshold) {
					break;
				}			
				
				// Language bias test
				if (query.cardinalityForRelation(relation) >= recursivityLimit) {
					continue;
				}
				
				if (bodyExcludedRelations != null 
						&& bodyExcludedRelations.contains(relation)) {
					continue;
				}
				
				if (bodyTargetRelations != null 
						&& !bodyTargetRelations.contains(relation)) {
					continue;
				}
				
				newEdge[1] = relation;
				//Before adding the edge, verify whether it leads to the hard case
				if(containsHardCase(query, newEdge))
					continue;
				
				Rule candidate = query.addAtom(newEdge, cardinality);
				List<ByteString[]> recursiveAtoms = candidate.getRedundantAtoms();
				if(!recursiveAtoms.isEmpty()){
					if(canAddInstantiatedAtoms()){
						for(ByteString[] triple: recursiveAtoms){										
							if(!KB.isVariable(triple[danglingPosition])){
								candidate.getTriples().add(
										KB.triple(newEdge[danglingPosition], 
										KB.DIFFERENTFROMbs, 
										triple[danglingPosition]));
							}
						}
						long finalCardinality;
						if(boundHead){
							//Single variable in head
							finalCardinality = this.kb.countDistinct(candidate.getFunctionalVariable(), candidate.getTriples());
						}else{
							//Still pending
							finalCardinality = this.kb.countProjection(candidate.getHead(), candidate.getAntecedent());
						}
						
						if(finalCardinality < minSupportThreshold)
							continue;
						
						candidate.setSupport(finalCardinality);
					}
				}
				
				candidate.setHeadCoverage(candidate.getSupport() / this.headCardinalities.get(candidate.getHeadRelation()));
				candidate.setSupportRatio(candidate.getSupport() / this.kb.size());
				candidate.addParent(query);			
				output.add(candidate);
				//System.out.println("Od adding " + candidate +  " support:  " + candidate.getSupport() + " from rule " + query);
			}
		}
		
	}
	
	/**
	 * It adds to the output all the rules resulting from adding dangling atom instantiation of "edge"
	 * to the query.
	 * @param query
	 * @param edge
	 * @param minSupportThreshold Minimum support threshold.
	 * @param output
	 */
	@Override
	protected void getDanglingAtoms(Rule query, ByteString[] edge, double minSupportThreshold, Collection<Rule> output) {
		List<ByteString> joinVariables = Arrays.asList(query.getFunctionalVariable());
		getDanglingAtoms(query, edge, minSupportThreshold, joinVariables, output);
	}
	
	protected void getInstantiatedAtoms(Rule query, Rule parentQuery, 
			int bindingTriplePos, int danglingPosition, double minSupportThreshold, Collection<Rule> output) {
		ByteString[] danglingEdge = query.getTriples().get(bindingTriplePos);
		Rule rewrittenQuery = null;
		if (!query.isEmpty() && this.enableQueryRewriting) {
			rewrittenQuery = rewriteProjectionQuery(query, bindingTriplePos, danglingPosition == 0 ? 2 : 0);
		}
		
		IntHashMap<ByteString> constants = null;
		if(rewrittenQuery != null){
			long t1 = System.currentTimeMillis();		
			constants = this.kb.countProjectionBindings(rewrittenQuery.getHead(), rewrittenQuery.getAntecedent(), danglingEdge[danglingPosition]);
			long t2 = System.currentTimeMillis();
			if((t2 - t1) > 20000 && this.verbose)
				System.err.println("countProjectionBindings var=" + danglingEdge[danglingPosition] + " in " + query + " (rewritten to " + rewrittenQuery + ") has taken " + (t2 - t1) + " ms");						
		}else{
			long t1 = System.currentTimeMillis();		
			constants = this.kb.countProjectionBindings(query.getHead(), query.getAntecedent(), danglingEdge[danglingPosition]);
			long t2 = System.currentTimeMillis();
			if((t2 - t1) > 20000 && this.verbose)
				System.err.println("countProjectionBindings var=" + danglingEdge[danglingPosition] + " in " + query + " has taken " + (t2 - t1) + " ms");			
		}
		
		int joinPosition = (danglingPosition == 0 ? 2 : 0);
		for(ByteString constant: constants){
			int cardinality = constants.get(constant);
			if(cardinality >= minSupportThreshold){
				ByteString[] targetEdge = danglingEdge.clone();
				targetEdge[danglingPosition] = constant;
				assert(KB.isVariable(targetEdge[joinPosition]));
				
				Rule candidate = query.instantiateConstant(bindingTriplePos, danglingPosition, constant, cardinality);				
				// Do this checking only for non-empty queries
				//If the new edge does not contribute with anything				
				if(candidate.getRedundantAtoms().isEmpty()){
					candidate.setHeadCoverage((double)cardinality / headCardinalities.get(candidate.getHeadRelation()));
					candidate.setSupportRatio((double)cardinality / (double)kb.size());
					candidate.addParent(parentQuery);
					//System.out.println("Oi adding candidate " + candidate);
					output.add(candidate);
				}
			}
		}
	}
	
	
	public void calculateConfidenceMetrics(Rule candidate) {
		super.calculateConfidenceMetrics(candidate);
		candidate.setPcaBodySize(candidate.getBodySize());
	}
	
	public boolean testConfidenceThresholds(Rule candidate) {		
		if(candidate.containsLevel2RedundantSubgraphs()) {
			return false;
		}	
		
		return candidate.getStdConfidence() >= minStdConfidence 
				&& candidate.getPcaConfidence() >= minPcaConfidence;
		
	}

}
