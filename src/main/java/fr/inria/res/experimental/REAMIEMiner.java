package fr.inria.res.experimental;


import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;

import amie.data.KB;
import amie.mining.AMIE;
import amie.mining.assistant.MiningAssistant;
import amie.rules.Metric;
import amie.rules.Rule;
import fr.inria.res.Atom;
import fr.inria.res.Condition;
import fr.inria.res.GlobalConfig;
import fr.inria.res.REMiner;
import fr.inria.res.complexity.KBDegreeBasedComplexityCalculator;
import javatools.datatypes.ByteString;
import javatools.datatypes.IntHashMap;

public class REAMIEMiner {
	protected KB kb;
	
	protected int maxAtoms;
	
	protected Map<String, Pair<Double, Double>> coefficients;
		
	protected boolean extended;
	
	public REAMIEMiner(KB kb, boolean extended) {
		this.kb = kb;
		this.maxAtoms = 4;	
		this.coefficients = Collections.emptyMap();
		this.extended = extended;
	}
		
	public REAMIEMiner(KB kb, Map<String, Pair<Double, Double>> coefficients, boolean extended) {
		this.kb = kb;
		this.maxAtoms = 4;
		this.coefficients = coefficients;
		this.extended = extended;		
	}
	
	public REAMIEMiner(KB kb, int maxAtoms, Map<String, Pair<Double, Double>> coefficients,
			boolean extended) {
		this.kb = kb;
		this.maxAtoms = maxAtoms;
		this.coefficients = coefficients;
		this.extended = extended;
	}
	
	public Condition mine(List<String> entities) throws Exception {
		MiningAssistant assistant = null;
		if (this.extended) {
			assistant = new REMinerExtendedMiningAssistant(this.kb);
		} else {
			assistant = new REMinerMiningAssistant(this.kb);
		}
		
		assistant.setAllowConstants(true);
		assistant.setDatalogNotation(true);
		assistant.setMaxDepth(4);
		assistant.setStdConfidenceThreshold(1.0);
		assistant.setPcaConfidenceThreshold(1.0);
		assistant.setEnablePerfectRules(true);
		
		AMIE amieMiner = new AMIE(assistant, 1, entities.size(), 
				Metric.Support, Runtime.getRuntime().availableProcessors());
		amieMiner.setSeeds(Arrays.asList(ByteString.of("target")));
		amieMiner.setRealTime(false);
		
		List<Rule> rules = amieMiner.mine();
		return filterByComplexity(rules);
	}

	private Condition filterByComplexity(List<Rule> rules) {
		Condition leastComplex = null;
		double minComplexity = Double.MAX_VALUE;
		for (Rule rule : rules) {
			// Do not add candidates with more than 2 variables
			if (rule.getVariables().size() > 2) 
				continue;
			
			Condition c = rule2Condition(rule);	
			double complexity = getComplexity(c);
			//System.out.println(c + " --- " + complexity);
			if (complexity < minComplexity) {
				minComplexity = complexity;
				leastComplex = c;
			}
		}

		return leastComplex;
	}
	
	private List<String> getPredicates(List<Atom> ancestors) {
		List<String> predicates = new ArrayList<String>();
		for (Atom atom : ancestors) {
			predicates.add(atom.getPredicat());
		}
		
		return predicates;
	}

	
	private double getComplexity(Condition c) {
		if (c.getComplexity() > -1.0)
			return c.getComplexity();
		
		Map<Atom, List<Atom>> atom2Ancestors = c.ancestorWalk();
		double cumulativeComplexity = 0.0;
		for (Atom atom : atom2Ancestors.keySet()) {
			List<Atom> ancestors = atom2Ancestors.get(atom);
			List<String> otherPredicates = getPredicates(ancestors);

			Pair<Double, Double> coeffsPredicate = coefficients.get("predicates");
			double npr = getCardinalityOfPredicate(atom.getPredicat());
			double complexityRelation = 0.0;
			if (coeffsPredicate == null) {
				complexityRelation = (-1 * Math.log(npr) / Math.log(2)) + 64.0;
			} else {
				complexityRelation = (coeffsPredicate.getKey() * Math.log(npr) / Math.log(2)) + coeffsPredicate.getValue();	
				
				double kr = 0.5;
				if (!otherPredicates.isEmpty()
						&& !containsNonSelectivePredicates(otherPredicates)) {
					kr = interceptAdjustmentForPredicates(otherPredicates);
					
				} else {
					kr = Math.pow(kr, otherPredicates.size());
				}
				
				double adjustment = Math.log(kr) / Math.log(2);
				complexityRelation += adjustment;
			}
			
			complexityRelation = Math.max(1.0, complexityRelation);
			
			double complexityObject = 0.0;
			if (atom.getObject() != null && !atom.getObject().equals("")) {
				double npc = 1.0;

				npc = getCardinalityOfConstant(atom.getPredicat(), atom.getObject()) + 1;			
				
				Pair<Double, Double> coeffsConstant = coefficients.get("constants");
				if (coeffsConstant == null) {
					complexityObject = -1 * Math.log(npc) / Math.log(2) + 64.0;
				} else {
					complexityObject = coeffsConstant.getKey() * Math.log(npc) / Math.log(2) + coeffsConstant.getValue();
				}
				
				// We have to adjust the intercept to account
				// for the size of the new ranking
				if (!otherPredicates.isEmpty()) {
					double adjustment = Math.log(interceptAdjustmentForConstants(atom.getPredicat(), 
							otherPredicates)) / Math.log(2);
					complexityObject += adjustment;
				}
				
				complexityObject = Math.max(1.0, complexityObject);
			}
			
			cumulativeComplexity += (complexityRelation + complexityObject);
		}
		
		c.setComplexity(cumulativeComplexity);
		return cumulativeComplexity;
	}
	
	private boolean containsNonSelectivePredicates(List<String> otherPredicates) {
		for (String predicate : otherPredicates) {
			if (kb.count(KB.triple("?s", predicate, "?o")) 
					>= KBDegreeBasedComplexityCalculator.thresholdForExactAdjustmentCalculation) {
				return true;
			}
		} 
		
		return false;
	}

	private double interceptAdjustmentForPredicates(List<String> otherPredicates) {
		List<ByteString[]> atoms = getQueryForConditionalRankingSize(otherPredicates);
		long numberOfEntitiesNewRanking = kb.countDistinct(ByteString.of("?p"), atoms);
		return (double) (numberOfEntitiesNewRanking + 1) / (kb.size(KB.Column.Relation) + 1);
	}

	private double interceptAdjustmentForConstants(String predicate, List<String> otherPredicates) {
		List<ByteString[]> atoms = getQueryForConditionalRankingOfConstantsSize(predicate, otherPredicates);
		long numberOfEntitiesNewRanking = kb.countDistinct(ByteString.of("?o"), atoms);
		List<ByteString[]> atomsDenom = new ArrayList<>(2);
		atomsDenom.add(KB.triple("?s", predicate, "?o"));
		double denominator = kb.countDistinct(ByteString.of("?o"), atomsDenom);
		return (double) (numberOfEntitiesNewRanking + 1) / (denominator + 1);
	}

	private List<ByteString[]> getQueryForConditionalRankingOfConstantsSize(String predicate,
			List<String> otherPredicates) {
		List<ByteString[]> atoms = new ArrayList<>();
		int variable = 0;
		atoms.add(KB.triple("?x" + variable, predicate, "?o"));
		
		for (String otherPredicate : otherPredicates) {
			atoms.add(KB.triple("?x" + (variable + 1), otherPredicate, "?x" + variable));
		}
		
		return atoms;
	}

	private List<ByteString[]> getQueryForConditionalRankingSize(List<String> otherPredicates) {
		List<ByteString[]> atoms = new ArrayList<>();
		int variable = 0;
		atoms.add(new ByteString[] {ByteString.of("?x"+ variable),
				ByteString.of("?p"), ByteString.of("?y")});
		
		for (String predicate : otherPredicates) {
			atoms.add(new ByteString[] {ByteString.of("?x" + (variable + 1)), 
					ByteString.of(predicate), ByteString.of("?x" + variable)});
		}
		
		return atoms;
	}

	private double getCardinalityOfConstant(String predicate, String object) {
		if (predicate.equals(GlobalConfig.typeRelation)) {
			// We implement a special treatment for classes
			List<ByteString[]> atoms = new ArrayList<>(2);
			atoms.add(KB.triple("?x", predicate, object));
			// This method will use the precomputed files ^_^
			return kb.countDistinct(ByteString.of("?x"), atoms);
		} else {
			return kb.count(KB.triple(object, "?p", "?o"));
		}
	}

	
	private double getCardinalityOfPredicate(String predicate) {
		return (double)kb.count(KB.triple("?s", predicate, "?o"));		
	}
	
	private Condition rule2Condition(Rule rule) {
		List<ByteString[]> antecedent = rule.getAntecedent();
		List<ByteString[]> bodyAtoms = new ArrayList<>();
		for (ByteString[] bAtom : antecedent) {
			if (!bAtom[1].equals(KB.DIFFERENTFROMbs) && 
					!bAtom[1].equals(KB.EQUALSbs)) {
				bodyAtoms.add(bAtom);
			}				
		}
		
		int nVars = numVariables(bodyAtoms);
		if (bodyAtoms.size() == 1) {
			Pair<String, String> atom = getPredicateAndObject(rule.getBody().get(0));
			return Condition.makeCondition(atom.getKey(), atom.getValue());
		} else if (bodyAtoms.size() == 2) {
			if (nVars == 1) {
				Pair<String, String> atom1 = getPredicateAndObject(bodyAtoms.get(0));
				Pair<String, String> atom2 = getPredicateAndObject(bodyAtoms.get(1));
				Condition c = new Condition(new Atom(null, atom1.getKey(), atom1.getValue(), null), 
						new Atom(null, atom2.getKey(), atom2.getValue(), null));
				return c;
			} else if (nVars == 2) {
				Pair<String, String> atom1 = getPredicateAndObject(bodyAtoms.get(0));
				Pair<String, String> atom2 = getPredicateAndObject(bodyAtoms.get(1));				
				
				if (atom1.getValue().equals(atom2.getValue())) {					
					return new Condition(
							new Atom(null, atom1.getKey(), "", "hiddenConstant"), 
							new Atom(null, atom2.getKey(), "", "hiddenConstant")
							);

				} else {
					Pair<String, String> atomWithHeadVar = null;
					Pair<String, String> atomWithOtherVariable = null;
					if (bodyAtoms.get(0)[0].equals(rule.getFunctionalVariable())) {
						atomWithHeadVar = atom1;
						atomWithOtherVariable = atom2;
					} else {
						atomWithHeadVar = atom2;
						atomWithOtherVariable = atom1;
					}
					Condition c = Condition.makeCondition(atomWithHeadVar.getKey(), "hiddenConstant");
					Atom a = new Atom("hiddenConstant", atomWithOtherVariable.getKey(), atomWithOtherVariable.getValue(), null);
					return c.chainAtomSO(a, c.atoms().keySet().iterator().next());
				}
					
			}
		} else if (bodyAtoms.size() == 3) {
			if (nVars == 1) {
				Pair<String, String> atom1 = getPredicateAndObject(rule.getBody().get(0));
				Pair<String, String> atom2 = getPredicateAndObject(rule.getBody().get(1));
				Pair<String, String> atom3 = getPredicateAndObject(rule.getBody().get(2));
				return new Condition(new Atom(null, atom1.getKey(), atom1.getValue(), null), 
						new Atom(null, atom2.getKey(), atom2.getValue(), null),
						new Atom(null, atom3.getKey(), atom3.getValue(), null));
			} else if (nVars == 2) {
				// Find the atom with the constant
				int index = -1;
				ByteString[] atomWithConstant = null;
				for (int i = 0; i < bodyAtoms.size(); ++i) {
					if (!KB.isVariable(bodyAtoms.get(i)[2])) {
						index = i;
					}
				}
				
				if (index == -1) {
					atomWithConstant = bodyAtoms.get(index);
					List<ByteString[]> newBodyAtoms = new ArrayList<ByteString[]>(bodyAtoms);
					newBodyAtoms.remove(index);

					// Look at the variable
					if (atomWithConstant[0].equals(rule.getFunctionalVariable())) {
						Atom c1 = new Atom(null, newBodyAtoms.get(0)[1].toString(), "" , "hiddenConstant");
						Atom c2 = new Atom(null, newBodyAtoms.get(1)[1].toString(), "" , "hiddenConstant");
						Atom c3 = new Atom(null, atomWithConstant[1].toString(),  atomWithConstant[2].toString(), null);
						return new Condition(c1, c2, c3);
					} else {
						Atom c1 = new Atom(null, newBodyAtoms.get(0)[1].toString(), "hiddenConstant" , null);
						Atom c2 = new Atom(null, newBodyAtoms.get(1)[1].toString(), "" , "hiddenConstant");
						Atom c3 = new Atom("hiddenConstant", atomWithConstant[1].toString(), atomWithConstant[2].toString(), null);
						Condition ctr = new Condition(c1, c2);
						return ctr.chainAtomSO(c3, c1);
					}
					
				} else {
					Pair<String, String> atom1 = getPredicateAndObject(rule.getBody().get(0));
					Pair<String, String> atom2 = getPredicateAndObject(rule.getBody().get(1));
					Pair<String, String> atom3 = getPredicateAndObject(rule.getBody().get(2));
					return new Condition(new Atom(null, atom1.getKey(), atom1.getValue(), null), 
							new Atom(null, atom2.getKey(), atom2.getValue(), null),
							new Atom(null, atom3.getKey(), atom3.getValue(), null));
				}
			}
		}

		return null;
	}

	private Pair<String, String> getPredicateAndObject(ByteString[] amieAtom) {
		return new MutablePair<String, String>(amieAtom[1].toString(), amieAtom[2].toString());
	}

	private int numVariables(List<ByteString[]> bodyAtoms) {
		IntHashMap<ByteString> varsCount = new IntHashMap<>();
		for (ByteString[] atom : bodyAtoms) {
			for (ByteString e : atom) {
				if (KB.isVariable(e)) {
					varsCount.increase(e);
				}
			}
		}
 		
		return varsCount.size();
	}

	public static void main(String args[]) throws Exception {
		long start = System.currentTimeMillis();
		List<String> lines = Files.readAllLines(Paths.get(args[1]),
	            Charset.forName("utf-8"));
		
		KB kb = new KB();
		kb.load(new File(args[0]));
		
		boolean extended = false;
		if (args.length > 4) {
			extended = Boolean.parseBoolean(args[4]);
		}
				
		final REAMIEMiner amieMiner = args.length > 2 ? 
				new REAMIEMiner(kb, REMiner.loadCoefficients(args[2]), extended) : 
					new REAMIEMiner(kb, extended);
		
		long initTime = System.currentTimeMillis() - start;
		System.out.println("Init time(ms)\t" + initTime);
        System.out.println("Line\tLength\tEntities\tSolution\tThere was a solution\tTime(ms)\tTimeout");
        int lineNumber = 1;
		ExecutorService executor = Executors.newCachedThreadPool();
        for (String line : lines) {
        	start = System.currentTimeMillis();
			String[] parts = line.split(" ");
			for (String entity : parts) {
				kb.add(KB.triple(ByteString.of(entity), ByteString.of("target"), ByteString.of("true")));
			}
			 
	    	start = System.currentTimeMillis();
			Future<Condition> future = executor.submit(new Callable<Condition>() {

			    public Condition call() throws Exception {
					 return amieMiner.mine(Arrays.asList(parts));
			    }
			});
			 
			Condition solution = null;
			boolean timeOut = false;
			try {
				solution = future.get(args.length > 3 ? Integer.parseInt(args[3]) : 30, TimeUnit.MINUTES); 
			} catch (TimeoutException e) {
				timeOut = true;
			} finally {
				future.cancel(true);
			} 
			long time = System.currentTimeMillis() - start;

						 
			for (String entity : parts) {
				kb.delete(ByteString.of(entity), ByteString.of("target"), ByteString.of("true"));
			}
			 
			System.out.println(lineNumber + "\t" 
			+ parts.length + "\t" + Arrays.asList(parts) + "\t" + solution + "\t" + 
					 (solution == null ? 0 : 1) + "\t" + time + "\t" + timeOut);
			++lineNumber;
        }
		executor.shutdownNow();

	}
}
