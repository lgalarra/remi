package fr.inria.res.complexity;

import java.util.List;
import java.util.Map;

import fr.inria.res.Atom;
import fr.inria.res.Condition;
import fr.inria.res.data.KB;

public class KBBasedImprovedComplexityCalculator extends KBBasedComplexityCalculator {

	public KBBasedImprovedComplexityCalculator(KB kb) {
		super(kb);
	}

	@Override
	public int compare(Condition o1, Condition o2) {
		return super.compare(o1, o2);
	}

	@Override
	public double getComplexity(Condition c) {
		if (c.getComplexity() == -1.0) {
			Map<Atom, List<Atom>> atom2Ancestors = c.ancestorWalk();
			c.setComplexity(getNumberOfBits(c, atom2Ancestors));
		}
		
		return c.getComplexity();
	}

	private double getNumberOfBits(Condition c, Map<Atom, List<Atom>> atom2Ancestors) {
		double nBits = 0;
		for (Atom atom : c.getSetAtoms()) {
			List<Atom> ancestors = atom2Ancestors.get(atom);
			List<String> otherPredicates = getPredicates(ancestors);
			Condition subCondition = c.search(atom);
			int multiplier = subCondition.getChildren(atom).size();
			if (multiplier == 0) multiplier = 1;

			double c1 = multiplier * getCardinalityOfPredicateGivenOtherPredicates(
					atom.getPredicat(), otherPredicates);
			
			nBits += KBBasedComplexityCalculator.complexityConstant - Math.log1p(c1) / Math.log(2);
			
			double c2 = 0.0;
			if (atom.getObject() != null && !atom.getObject().equals("")) {
				otherPredicates.add(0, atom.getPredicat());
				c2 = getCardinalityOfEntityGivenOtherPredicates(atom.getObject(), otherPredicates);
			}
			
			nBits += KBBasedComplexityCalculator.complexityConstant - Math.log1p(c2) / Math.log(2);
			
			for (Condition child : c.getChildren(atom)) {
				nBits += getNumberOfBits(child, atom2Ancestors);
			}
		}
		
		return nBits;
	}

}
