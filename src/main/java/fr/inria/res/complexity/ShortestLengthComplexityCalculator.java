package fr.inria.res.complexity;

import fr.inria.res.Condition;
import fr.inria.res.data.KB;

public class ShortestLengthComplexityCalculator extends KBBasedComplexityCalculator {

	public ShortestLengthComplexityCalculator (KB kb) {
		super(kb);
	}

	@Override
	public int compare(Condition o1, Condition o2) {
		return super.compare(o1, o2);
	}

	@Override
	public double getComplexity(Condition c) {
		return c.size();
	}
}
