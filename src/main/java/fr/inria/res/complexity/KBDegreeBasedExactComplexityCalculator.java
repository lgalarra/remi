package fr.inria.res.complexity;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.inria.res.Atom;
import fr.inria.res.Condition;
import fr.inria.res.GlobalConfig;
import fr.inria.res.data.KB;

public class KBDegreeBasedExactComplexityCalculator extends KBDegreeBasedComplexityCalculator {

	protected Map<String, Map<String, Double>> conditionalComplexityConstants;
	
	protected Map<String, Double> complexityPredicates;
	
	protected Map<String, Double> complexityConstants;
	
	public KBDegreeBasedExactComplexityCalculator(KB kb) {
		super(kb);
		conditionalComplexityConstants = new HashMap<>();
		complexityPredicates = new HashMap<>();
		complexityConstants = new HashMap<>();
		List<File> rankingFiles = listDirectory(GlobalConfig.getKBDirectory() + "/ranking_relations");
		for (File file : rankingFiles) {
			System.out.println("Loading " + file.getAbsolutePath());
			try {
				parseRelationRanking(file);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Done");
		}
		
		try {
			parseRanking(new File(GlobalConfig.getKBDirectory() + "/cached_relation_queries"), complexityPredicates);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			parseRanking(new File(GlobalConfig.getKBDirectory() + "/topentities-ranking"), complexityConstants);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public double getComplexity(Condition c) {
		if (c.getComplexity() > -1.0)
			return c.getComplexity();
		
		Map<Atom, List<Atom>> atom2Ancestors = c.ancestorWalk();
		double cumulativeComplexity = 0.0;
		for (Atom atom : atom2Ancestors.keySet()) {
			List<Atom> ancestors = atom2Ancestors.get(atom);
			List<String> otherPredicates = getPredicates(ancestors);
			Condition subCondition = c.search(atom);
			int multiplier = subCondition.getChildren(atom).size();
			if (multiplier == 0) multiplier = 1;
			
			double degreeRelation = getCardinalityOfPredicateGivenOtherPredicates(atom.getPredicat(), otherPredicates) + 1;
			double complexityRelation =  0.0;
			if (this.complexityPredicates.containsKey(atom.getPredicat())) {
				complexityRelation = this.complexityPredicates.get(atom.getPredicat());
			} else {
				complexityRelation = multiplier * ((this.defaultPredicateSlope * Math.log(degreeRelation) / Math.log(2)) 
						+ this.defaultPredicateIntercept);
			}
			
			double degreeConstant = 1.0;			
			double slopeConstants = this.defaultConstantsGivenPredicateSlope;
			double interceptConstants = this.defaultConstantsGivenPredicateIntercept;
			double complexityObject = 0.0;
			if (atom.getObject() != null && !atom.getObject().equals("")) {
				Map<String, Double> rankingPredicate = conditionalComplexityConstants.get(atom.getPredicat());
				if (rankingPredicate != null) {
					Double complexity = rankingPredicate.get(atom.getObject());
					if (complexity != null) {
						complexityObject = complexity.doubleValue();
					} else {
						degreeConstant = getCardinalityOfConstant(atom.getPredicat(), atom.getObject()) + 1;			
						complexityObject = slopeConstants * Math.log(degreeConstant) / Math.log(2) + interceptConstants;
					}
				} else {
					degreeConstant = getCardinalityOfConstant(atom.getPredicat(), atom.getObject()) + 1;			
					complexityObject = slopeConstants * Math.log(degreeConstant) / Math.log(2) + interceptConstants;					
				}
			}
			
			cumulativeComplexity += (complexityRelation + complexityObject);
		}
		
		c.setComplexity(cumulativeComplexity);
		return cumulativeComplexity;
	}
	
	private void parseRanking(File file, Map<String, Double> complexityMap) throws IOException {
		int lineN = 1;
		List<String> lines = Files.readAllLines(Paths.get(file.getAbsolutePath()),
				Charset.defaultCharset());
		for (String line : lines) {
			String parts[] = line.split(" ");
			if (parts.length > 1) {					
				complexityMap.put(parts[0], Math.log(lineN) / Math.log(2));
				++lineN;
			}
		}
	}

	private void parseRelationRanking(File file) throws IOException {
		List<String> lines = Files.readAllLines(Paths.get(file.getAbsolutePath()),
				Charset.defaultCharset());
		int lineN = 1;
		String predicate = null;
		for (String line : lines) {
			String parts[] = line.split(" ");
			if (lineN == 1) {		
				predicate = parts[0].replaceAll("<", "").replaceAll(">", "").trim();
				conditionalComplexityConstants.put(predicate, new HashMap<>());
				++lineN;
			} else {
				if (parts.length > 1 && !parts[0].isEmpty()) {
					if (parts[0].startsWith("\""))
						break;
					
					conditionalComplexityConstants.get(predicate).put(parts[0], Math.log(lineN) / Math.log(2));
					++lineN;
				}
			}
		}
	}

	private List<File> listDirectory(String directoryName) {
	    File directory = new File(directoryName);
	    List<File> files = new ArrayList<>();
	    // Get all the files from a directory.
	    File[] fList = directory.listFiles();
	    for (File file : fList) {
	        if (file.getName().startsWith("ranking_")) {
		    	if (file.isFile()) {
		            files.add(file);
		        }
	        }
	    }
	    
	    return files;
	}

}
