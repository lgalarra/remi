package fr.inria.res.complexity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import fr.inria.res.Atom;
import fr.inria.res.Condition;
import fr.inria.res.GlobalConfig;
import fr.inria.res.data.KB;

public class KBBasedComplexityCalculator extends ComplexityCalculator {
	protected KB kb;
	
	public static double complexityConstant = Long.SIZE * 8;
	
	public static String typeString = GlobalConfig.typeRelation;
	
	public KBBasedComplexityCalculator(KB kb) {
		this.kb = kb;	
	}
	
	@Override
	public int compare(Condition o1, Condition o2) {
		double c1 = getComplexity(o1);
		double c2 = getComplexity(o2);
		int cmp = Double.compare(c1, c2);
		if (cmp == 0) {
			return o1.toString().compareTo(o2.toString());
		} else {
			return cmp;
		}
	}
	

	/**
	 * Computes the term of the chain rule for object values given its precedent 
	 * predicates in the path.
	 * @param object
	 * @param predicates
	 * @return
	 */
	protected double getCardinalityOfEntityGivenOtherPredicates(String object, List<String> predicates) {
		int variable = 0;
		String[] firstAtom = new String[] {"?x"+variable, predicates.get(0), object};		
		List<String[]> atoms = new ArrayList<>();
		atoms.add(firstAtom);
		for (String otherPredicate : predicates.subList(1, predicates.size())) {
			atoms.add(new String[] {"?x"+(variable+1), otherPredicate, "?x"+(variable)});
			++variable;
		}
		
		return kb.countDistinct("?x0", atoms);
	}

	/**
	 * Computes the term of the chain rule for predicate values given its precedent 
	 * predicates in the path.
	 * @param object
	 * @param predicates 
	 * @return
	 */
	protected double getCardinalityOfPredicateGivenOtherPredicates(String predicate, 
			List<String> otherPredicates) {
		int variable = 0;
		List<String[]> atoms = new ArrayList<>();
		if (!predicate.equals(typeString) || otherPredicates.isEmpty()) {
			String[] firstAtom = new String[] {"?x"+variable, predicate, "?y"};
			atoms.add(firstAtom);
		}
		
		for (String otherPredicate : otherPredicates) {
			atoms.add(new String[] {"?x"+(variable + 1), otherPredicate, "?x"+(variable)});
			++variable;
		}
		
		return kb.countDistinct("?x0", atoms);
	}

	protected List<String> getPredicates(List<Atom> ancestors) {
		List<String> predicates = new ArrayList<String>();
		for (Atom atom : ancestors) {
			predicates.add(atom.getPredicat());
		}
		
		return predicates;
	}

	@Override
	public double getComplexity(Condition c) {
		if (c.getComplexity() > -1.0)
			return c.getComplexity();
		
		Map<Atom, List<Atom>> atom2Ancestors = c.ancestorWalk();
		double cumulativeCardinality = 0.0;
		for (Atom atom : atom2Ancestors.keySet()) {
			List<Atom> ancestors = atom2Ancestors.get(atom);
			List<String> otherPredicates = getPredicates(ancestors);
			Condition subCondition = c.search(atom);
			int multiplier = subCondition.getChildren(atom).size();
			if (multiplier == 0) multiplier = 1;

			double c1 = multiplier * getCardinalityOfPredicateGivenOtherPredicates(
					atom.getPredicat(), otherPredicates);
			
			double c2 = 0.0;
			if (atom.getObject() != null && !atom.getObject().equals("")) {
				otherPredicates.add(0, atom.getPredicat());
				c2 = getCardinalityOfEntityGivenOtherPredicates(atom.getObject(), otherPredicates);
			}
			
			cumulativeCardinality += (c1 + c2);
		}
		
		return complexityConstant - Math.log1p(cumulativeCardinality) / Math.log(2);
	}

	@Override
	public double getComplexity(String constant) {
		return 0;
	}

}
