package fr.inria.res.complexity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import fr.inria.res.Atom;
import fr.inria.res.Condition;
import fr.inria.res.GlobalConfig;
import fr.inria.res.data.KB;

public class KBEntropyBasedComplexityCalculator extends KBBasedComplexityCalculator {
	
	public KBEntropyBasedComplexityCalculator(KB kb) {
		super(kb);
	}
	
	@Override
	public double getComplexity(Condition c) {
		if (c.getComplexity() > -1.0)
			return c.getComplexity();
		
		Map<Atom, List<Atom>> atom2Ancestors = c.ancestorWalk();
		double cumulativeComplexity = 0.0;
		for (Atom atom : atom2Ancestors.keySet()) {
			List<Atom> ancestors = atom2Ancestors.get(atom);
			List<String> otherPredicates = getPredicates(ancestors);
			Condition subCondition = c.search(atom);
			int multiplier = subCondition.getChildren(atom).size();
			if (multiplier == 0) multiplier = 1;

			double npr = getCardinalityOfPredicateGivenOtherPredicates(atom.getPredicat(), otherPredicates);
			double dpr = 1.0;
			if (otherPredicates.size() > 1) {
				String closestAncestor = otherPredicates.get(otherPredicates.size() - 1);
				otherPredicates.remove(otherPredicates.size() - 1);
				dpr = getCardinalityOfPredicateGivenOtherPredicates(closestAncestor, otherPredicates);
			} else {
				dpr = kb.size();
			}
			
			double pr = npr / dpr;
			double complexityRelation = 0.0;
			if (pr != 1.0) {
				complexityRelation = -multiplier * pr * Math.log(pr) / Math.log(2);
			}
			
			double pc = 1.0;
			if (atom.getObject() != null && !atom.getObject().equals("")) {
				otherPredicates.add(0, atom.getPredicat());
				double npc = getCardinalityOfConstant(atom.getPredicat(), atom.getObject()) + 1;
				double dpc = getPOCardinalityOfPredicates(otherPredicates) + 1;
				pc =  npc / dpc;
			}
			
			double complexityObject = 0.0;
			if (pc != 1.0) {				
				complexityObject = -pc * Math.log(pc) / Math.log(2);
			}
			System.out.println(c + "cr=" + complexityRelation + " co=" + complexityObject);
			cumulativeComplexity += (complexityRelation + complexityObject);
		}
		
		c.setComplexity(cumulativeComplexity);
		return cumulativeComplexity;
	}

	protected double getCardinalityOfConstant(String predicate, String object) {
		if (predicate.equals("<" + GlobalConfig.typeRelation + ">")) {
			// We implement a special treatment for classes
			List<String[]> atoms = new ArrayList<>(2);
			atoms.add(new String[] {"?x", predicate, object});
			// This method will use the precomputed files ^_^
			return kb.countDistinct("?x", atoms);
		} else {
			return kb.countDistinctPO(new String[] {object, "", ""});
		}
	}
	
	@Override
	protected double getCardinalityOfPredicateGivenOtherPredicates(String predicate, 
			List<String> otherPredicates) {
		int variable = 0;
		List<String[]> atoms = new ArrayList<>();
		
		if (!predicate.equals(GlobalConfig.typeRelation)) {			
			String[] firstAtom = new String[] {"?s", predicate, "?o" + variable};
			atoms.add(firstAtom);
			
			for (String otherPredicate : otherPredicates) {
				atoms.add(new String[] {"?o"+(variable + 1), otherPredicate, "?o"+(variable)});
				++variable;
			}
			
			if (predicate.equals("http://dbpedia.org/ontology/country")) {
				System.out.println(" ?s ?o0 ");
				for (String[] a : atoms) {
					System.out.println(Arrays.toString(a));
				}
			}
				
			
			return kb.countDistinct("?s", "?o0", atoms);
		} else {
			return kb.size();
		}
		
	}
	
	/**
	 * Computes the term of the chain rule for object values given its precedent 
	 * predicates in the path.
	 * @param object
	 * @param predicates
	 * @return
	 */
	protected double getPOCardinalityOfPredicates(List<String> predicates) {
		int variable = 0;
		String firstAtom[] = null;
		boolean isNotTypeRelation = !predicates.get(0).equals(GlobalConfig.typeRelation);
		if (isNotTypeRelation) {
			firstAtom = new String[] {"?s"+variable, "?p", "?o"};
		} else {
			firstAtom = new String[] {"?o", "?p", "?s"+variable};
		}
		
		List<String[]> atoms = new ArrayList<>();
		atoms.add(firstAtom);
		
		for (String otherPredicate : predicates) {
			atoms.add(new String[] {"?s"+(variable+1), otherPredicate, "?s"+(variable)});
			++variable;
		}
		
		if (isNotTypeRelation) {
			return kb.countDistinct("?p", "?o", atoms);
		} else {
			return kb.countDistinct("?o", atoms);
		}
	}


}
