package fr.inria.res;

public enum SetRelationCase {
	EQUALITY, SUPERSET, SUBSET, NOCONTAINMENT
}
