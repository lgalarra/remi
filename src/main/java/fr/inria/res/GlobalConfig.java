package fr.inria.res;

/**
 * This class stores information about the global configuration of the system
 * (variables that will not change during the program execution)
 * @author luis
 *
 */
public class GlobalConfig {
	
	private static int concurrency;
	
	private static String kbDirectory;
	
	private static String kbPath;
	
	private static String topEntitiesPath;

	private static String queryTypeCachePath;
	
	private static String queryRelationCachePath;
	
	private static String prefixesPath;
	
	private static String coefficientsPath;
	
	private static String queryRelationObjectCountCachePath;
	
	public static String typeRelation = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type";
	
	public static void setKbPath(String folderPath, String filename) {
		kbDirectory = folderPath;
		kbPath = folderPath + "/" + filename;
		topEntitiesPath = folderPath + "/topentities"; 
		queryTypeCachePath = folderPath + "/cached_queries";
		queryRelationCachePath = folderPath + "/cached_relation_queries";
		queryRelationObjectCountCachePath = folderPath + "/cached_relation_object_counts";
		prefixesPath = folderPath + "/prefixes";
		coefficientsPath = folderPath + "/ranking_relations/coefficients";
	}
	
	public static void setKbPath(String folderPath, String filename, boolean usePageRank) {
		setKbPath(folderPath, filename);
		if (usePageRank) {
			topEntitiesPath = folderPath + "/topentities_pagerank"; 
			coefficientsPath = folderPath + "/ranking_relations_pagerank/coefficients";
		}	
	}
	
	public static String getKBDirectory() {
		return kbDirectory;
	}
	
	public static String getKbPath() {
		return kbPath;
	}
	
	public static String getQueryTypeCachePath() {
		return queryTypeCachePath;
	}
	
	public static String getQueryRelationCachePath() {
		return queryRelationCachePath;
	}
	
	public static String getTopEntitiesPath() {
		return topEntitiesPath;
	}
	
	public static String getPrefixesPath() {
		return prefixesPath;
	}
	
	public static String getCoefficientsPath() {
		return coefficientsPath;
	}

	public static void setConcurrency(int nbProcessors) {
		concurrency = nbProcessors;		
	}
	
	public int getConcurrency() {
		return concurrency;
	}

	public static String getQueryRelationObjectCountCachePath() {
		return queryRelationObjectCountCachePath;
	}

}
