package fr.inria.res.tools;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.inria.res.Condition;
import fr.inria.res.GlobalConfig;
import fr.inria.res.REMiner;
import fr.inria.res.Solution;
import fr.inria.res.complexity.ComplexityCalculator;
import fr.inria.res.complexity.ComplexityFunction;
import fr.inria.res.data.HDTKB;
import fr.inria.res.data.KB;

/**
 * class to execute function from REMiner to output in "samplerOutputFile" random condition, random solution, 
 * time to execute and complexity of the target entities
 * 
 * You have to choose a number of solution you want before the algorithm is stopping with the param "compteur"
 * 
 * @author jdelauna
 *
 */

public class REMinerOutputSampler {

	public REMinerOutputSampler(KB kb) throws IOException, InterruptedException {
		PrintWriter samplerOutputFile = new PrintWriter(new DataOutputStream(
				new BufferedOutputStream(new FileOutputStream(new File("src/data/dbpedia/samplerOutputFile.tsv")))));
		BufferedReader samplerFile = null;

		try {
			samplerFile = new BufferedReader(new FileReader("src/data/dbpedia/samplerFile"));
		} catch (FileNotFoundException exc) {
			System.out.println("Erreur d'ouverture");
		}
		List<List<String>> listTotalEntities = new ArrayList<>();
		String line;
		while ((line = samplerFile.readLine()) != null) {
			listTotalEntities.add(parserFileSampler(line));
		}
		REMiner re = new REMiner(kb, true, true, true, true, ComplexityFunction.DEGREE_BASED, 4, false, false);
		ComplexityCalculator cr = re.getComplexityCalculator(kb, ComplexityFunction.DEGREE_BASED);

		samplerOutputFile.println("Target entities" + "\t" + "Solution" + "\t" + "Solution trouvees" );
		int compteur = 0;
		for (int i = 0; i < listTotalEntities.size(); i++) {
			System.out.println(listTotalEntities.get(i));
			// Count the number of responses before stopping
			if (compteur == 5) {
				samplerOutputFile.print("pourcentage de réponse trouvées : " + "\t" + (((float) compteur / (float) i) * 100));
				break;
			}
			Set<Condition> setCond = new LinkedHashSet<>();
			setCond.addAll(re.intersectEntities(listTotalEntities.get(i)));
			if (!setCond.isEmpty()) {
				List<List<Condition>> listSolution = new ArrayList<>();
				//List<Condition> listCond = re.mineExhaustivelyInParallel(listTotalEntities.get(i), setCond);
				Map<Long, String> time = new LinkedHashMap<>();
				List<Condition> rankedCondition = new ArrayList<>();
				Condition[] listCond = re.sortConditionsInParallel(setCond);
				//List<Condition> listCond = re.mineExperimental(listTotalEntities.get(i), setCond, listSolution, time, rankedCondition );
				if (listCond.length != 0) {
					compteur++;
					System.out.println();
					System.out.println();
					System.out.println();
					System.out.println(compteur);
					System.out.println();
					System.out.println();
					System.out.println();
					Double complexity = 0.;
					for (Condition cond : listCond) {
						complexity += cond.getComplexity();
					}
					samplerOutputFile.println(
							listTotalEntities.get(i) + "\t" + getRandomRanked(listCond) + "\t" + getRandomSolutions(listSolution));
				}
			}
		}

		samplerOutputFile.close();
	}

	/**
	 * Return 5 random condition including the top3, the last condition and a random one
	 * @param listCond
	 * @return
	 */
	private String getRandomRanked(Condition[] listCond) {
		String randomRankedCondition = "";
		int size = listCond.length;
		if (size > 5) {
			int random = (int) ((size - 4) - (Math.random() * (listCond.length - 5)));
			randomRankedCondition += "Condition 1 : " + listCond[listCond.length - 1];
			randomRankedCondition += "Condition 2 : " + listCond[listCond.length- 2];
			randomRankedCondition += "Condition 3 : " + listCond[listCond.length- 3];
			randomRankedCondition += "Condition aléatoire : " + listCond[random];
			randomRankedCondition += "dernière Condition : " + listCond[0];
			return randomRankedCondition;
		} else
			return listCond.toString();
	}

	/**
	 * Get total complexity for a list of string
	 * @param list
	 * @param cr
	 * @return
	 */
	private Double getTotalComplexity(List<String> list, ComplexityCalculator cr) {
		Double totalComplexity = 0.;
		for (int i = 0; i < list.size(); i++) {
			totalComplexity += cr.getComplexity(list.get(i));
		}
		return totalComplexity;
	}

	/**
	 * Return 5 random condition including the top3, the last condition and a random one
	 * @param rankedCondition
	 * @return
	 */
	private String getRandomRanked(List<Condition> rankedCondition) {
		String randomRankedCondition = "";
		int size = rankedCondition.size();
		if (size > 5) {
			int random = (int) ((size - 4) - (Math.random() * (rankedCondition.size() - 5)));
			randomRankedCondition += "Condition 1 : " + rankedCondition.get(rankedCondition.size() - 1);
			randomRankedCondition += "Condition 2 : " + rankedCondition.get(rankedCondition.size() - 2);
			randomRankedCondition += "Condition 3 : " + rankedCondition.get(rankedCondition.size() - 3);
			randomRankedCondition += "Condition aléatoire : " + rankedCondition.get(random);
			randomRankedCondition += "dernière Condition : " + rankedCondition.get(0);
			return randomRankedCondition;
		} else
			return rankedCondition.toString();
	}

	/**
	 * Return 5 random solution including the top3, the last solution and a random one
	 * @param listSolution
	 * @return
	 */
	private String getRandomSolutions(List<List<Condition>> listSolution) {
		String listSol = "";
		if (listSolution.size() > 5) {
			int random = (int) (Math.random() * listSolution.size());
			listSol += " 1 :  " + listSolution.get(listSolution.size() - 1);
			listSol += " 2 :  " + listSolution.get(listSolution.size() - 2);
			listSol += " 3 :  " + listSolution.get(listSolution.size() - 3);
			listSol += " 4 :  " + listSolution.get(random);
			listSol += " 5 :  " + listSolution.get(0);
			return listSol;
		} else
			return listSolution.toString();
	}

	/**
	 * Parser for the target entities to print them clearly
	 * @param line
	 * @return
	 */
	protected List<String> parserFileSampler(String line) {
		List<String> listStr = new ArrayList<>();
		Pattern p = Pattern.compile(
				"(http(s)?://([a-zA-Z.0-9]+/)?([-_0-9a-zA-Z]+/)*)*([-_'.\\p{javaLowerCase}\\p{javaUpperCase}\\\\\"()@]+([\\p{Punct}0-9a-zA-Z]*)?)");
		Matcher m = p.matcher(line);
		while (m.find()) {
			listStr.add(m.group(0));
		}
		return listStr;
	}

	public static void main(String[] args) throws IOException, InterruptedException {
		KB kb;
		String fileName = "src/data/dbpedia/dbpedia.hdt";
		GlobalConfig.setKbPath("src/data/dbpedia/", "dbpedia.hdt");
		kb = new HDTKB(fileName);
		REMinerOutputSampler re = new REMinerOutputSampler(kb);
	}

}
