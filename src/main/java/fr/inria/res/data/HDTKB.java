package fr.inria.res.data;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.rdfhdt.hdt.exceptions.NotFoundException;
import org.rdfhdt.hdt.hdt.HDT;
import org.rdfhdt.hdt.hdt.HDTManager;
import org.rdfhdt.hdt.triples.IteratorTripleString;
import org.rdfhdt.hdt.triples.TripleString;
import org.rdfhdt.hdtjena.HDTGraph;

import fr.inria.res.Atom;
import fr.inria.res.Condition;
import fr.inria.res.GlobalConfig;

public class HDTKB extends KB {

	private String sourceFile;

	private HDT data;
	
	private Map<Long, Model> dataModels;

	private LRUQueryCache<Long> countDistinctCache;

	private LRUQueryCache<Set<String>> selectDistinctCache;
	
	private LRUQueryCache<Long> countDistinctPOCache;

	private Map<String, Long> cachedTypeCardinalities;
	
	private Map<String, Long> cachedRelationCardinalities;
	
	private Map<String, Long> cachedRelationObjectCountCardinalities;
	
	private Map<String, String> prefixesMap;
	
	private long numberOfPredicates;

	
	public HDTKB(String fileName) throws IOException {
		this(fileName, true);
	}
	
	public HDTKB(String fileName, boolean useCache) throws IOException {
		// Load HDT file using the hdt-java library
		numberOfPredicates = 0;
		sourceFile = fileName;
		data = HDTManager.loadIndexedHDT(fileName, null);
		HDTGraph graph = new HDTGraph(data);
		dataModels = new HashMap<>();
		Model dataModel = ModelFactory.createModelForGraph(graph);
		dataModels.put(Thread.currentThread().getId(), dataModel);
		countDistinctCache = new LRUQueryCache<Long>(1000);
		selectDistinctCache = new LRUQueryCache<Set<String>>(300);
		countDistinctPOCache = new LRUQueryCache<Long>(300);
				
		cachedTypeCardinalities = new HashMap<>();
		cachedRelationCardinalities = new HashMap<>();
		cachedRelationObjectCountCardinalities = new HashMap<>();
		prefixesMap = new LinkedHashMap<>();
		if (useCache) {
			loadQueryCache();
			loadPrefixesMap();
		}
	}
	
	private void loadPrefixesMap() {
		try {
			List<String> lines = Files.readAllLines(Paths.get(GlobalConfig.getPrefixesPath()),
					Charset.defaultCharset());
			for (String line : lines) {
				String parts[] = line.split("\\s");
				prefixesMap.put(parts[0], parts[1]);
			}
			
		} catch (IOException e) {
			System.err.println(e.getStackTrace());
		}
	}

	private synchronized Model getModel() {
		Model model = dataModels.get(Thread.currentThread().getId());
		if (model == null) {
			model = ModelFactory.createModelForGraph(new HDTGraph(data));
			dataModels.put(Thread.currentThread().getId(), model);
			return model;
		} else {
			return dataModels.get(Thread.currentThread().getId());
		}
	}

	/**
	 * Separate each lines of the file Cache_Query into the map queryCache composed
	 * of the class of predicate "rdf type" and number of occurrence of each
	 * 
	 * @param queryCacheLines
	 */
	private long parseQueryCache(List<String> queryCacheLines, Map<String, Long> cache) {
		long count = 0;
		for (String str : queryCacheLines) {
			String parts[] = str.split("\\s");
			cache.put(parts[0], Long.parseLong(parts[1]));
			++count;
		}
		
		return count;
	}

	/**
	 * This function load the cache_Queries file and modify the map queryCache
	 * 
	 * @throws IOException
	 */
	private void loadQueryCache() {
		try {
			List<String> lines = Files.readAllLines(Paths.get(GlobalConfig.getQueryTypeCachePath()),
					Charset.defaultCharset());
			parseQueryCache(lines, cachedTypeCardinalities);
		} catch (IOException e) {
			System.err.println(e.getStackTrace());
		}
		
		try {
			List<String> lines = Files.readAllLines(Paths.get(GlobalConfig.getQueryRelationCachePath()),
					Charset.defaultCharset());
			numberOfPredicates = parseQueryCache(lines, cachedRelationCardinalities);
		} catch (IOException e) {
			System.err.println(e.getStackTrace());
		}
		
		try {
			List<String> lines = Files.readAllLines(Paths.get(GlobalConfig.getQueryRelationObjectCountCachePath()),
					Charset.defaultCharset());
			parseQueryCache(lines, cachedRelationObjectCountCardinalities);
		} catch (IOException e) {
			System.err.println(e.getStackTrace());
		}
	}

	public HDTKB(HDTKB kb) throws IOException {
		this(kb.sourceFile);
	}

	@Override
	public Map<String, Set<String>> selectDistinctPO(String[] atom) {
		Map<String, Set<String>> results = new LinkedHashMap<>();

		// Search pattern: Empty string means "any"
		IteratorTripleString it = null;
		try {
			it = data.search(atom[0], atom[1], atom[2]);
		} catch (NotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		while (it.hasNext()) {
			TripleString ts = it.next();
			String pred = ts.getPredicate().toString();
			Set<String> objValues = results.get(pred);
			if (objValues == null) {
				objValues = new LinkedHashSet<String>();
				results.put(pred, objValues);
			}
			objValues.add(ts.getObject().toString());
		}

		return results;
	}

	@Override
	public Set<String> selectDistinctS(String[] atom) {
		Set<String> results = new LinkedHashSet<>();

		// Search pattern: Empty string means "any"
		IteratorTripleString it = null;
		try {
			it = data.search(atom[0], atom[1], atom[2]);
		} catch (NotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		while (it.hasNext()) {
			TripleString ts = it.next();
			results.add(ts.getSubject().toString());
		}

		return results;
	}

	@Override
	public Set<String> selectDistinctO(String[] atom) {
		Set<String> results = new LinkedHashSet<>();
		
		// Search pattern: Empty string means "any"
		IteratorTripleString it = null;
		try {
			it = data.search(atom[0], atom[1], atom[2]);
		} catch (NotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		while (it.hasNext()) {
			TripleString ts = it.next();
			results.add(ts.getObject().toString());
		}

		return results;
	}
	
	@Override
	public double countDistinctPO(String[] atom) {
		Long answer = countDistinctPOCache.get(atom[0]);
		if (answer == null) {
			answer = (long) selectDistinctPO(atom).size();
			countDistinctPOCache.put(atom[0], answer);
		}
		
		return (double)answer;
	}

	@Override
	public Set<String> selectDistinct(Condition condition) {
		String queryString = HDTKB.condition2Query(condition, "?x0");
		Set<String> answer = selectDistinctCache.get(queryString); 	
				
		if (answer == null) {
			answer = execute(queryString, "?x0");
			selectDistinctCache.put(queryString, answer);
		}
				
		return answer;
	}

	private Set<String> execute(String queryString, String variable) {
		String cleanVariable = variable.replace("?", "");
		Set<String> resultSet = new LinkedHashSet<>();
		Query query = QueryFactory.create(queryString);
		Model model = getModel();
		try (QueryExecution qexec = QueryExecutionFactory.create(query, model)) {
			ResultSet results = qexec.execSelect();
			for (; results.hasNext();) {
				QuerySolution soln = results.nextSolution();
				resultSet.add(soln.get(cleanVariable).toString());
			}
		}

		return resultSet;
	}

	public Model getDataSource() {
		return getModel();
	}

	public static String condition2Query(Condition condition, String variable) {
		List<String[]> atoms = condition2Atoms(condition, variable);
		return atoms2Query(atoms, variable);

	}

	private static String atoms2Query(List<String[]> atoms, String variable) {
		StringBuilder strBuilder = new StringBuilder();
		strBuilder.append("SELECT DISTINCT " + variable + " WHERE { ");

		for (String[] atom : atoms) {
			boolean addDot = true;
			for (String val : atom) {				
				if (val.startsWith("?") || val.startsWith("<")) {
					strBuilder.append(val);
				} else {
					String oEntity = wrapEntity(val);
					strBuilder.append(oEntity);
					if (oEntity.contains(". FILTER")) {
						addDot = false;
					}
				}
				strBuilder.append(" ");
			}
			
			if (addDot) {
				strBuilder.append(" . ");
			}
		}

		strBuilder.append("}");

		return strBuilder.toString();
	}

	public static List<String[]> condition2Atoms(Condition condition, String variable) {
		List<String[]> queryResult = new ArrayList<>();

		for (Atom atom : condition.getSetAtoms()) {
			List<Condition> subConditions = condition.getChildren(atom);
			if (subConditions.isEmpty()) {
				queryResult.add(atom2Query(atom, variable, 1));
			} else {
				int suffixForObjectVariable = 1;
				for (Condition subCondition : subConditions) {
					String[] bsAtom = atom2Query(atom, variable, suffixForObjectVariable);
					queryResult.add(bsAtom);
					String newVariable = bsAtom[2].toString();
					queryResult.addAll(condition2Atoms(subCondition, newVariable));
					++suffixForObjectVariable;
				}
			}
		}

		return queryResult;
	}

	// I change it to a public method for notation of Condition
	public static String[] atom2Query(Atom atom, String subjectVariable, int suffixForObjectVariable) {
		String[] result = new String[] { subjectVariable, "<" + atom.getPredicat() + ">", "" };
		String object = atom.getObject();
		if (object.equals("")) {
			result[2] = getNextVariable(subjectVariable, suffixForObjectVariable);
		} else {
			result[2] = wrapEntity(object);
		}
		return result;
	}

	private static String wrapEntity(String object) {
		if (object.startsWith("http://")) {
			return "<" + object + ">";
		} else {
			// It is a constant
			int indexOfType = object.indexOf("\"^^http://");
			if (indexOfType != -1) {
				String newObject = object.replace("^^http://", "^^<http://") + ">";
				String variable = "?xy" + (new Random()).nextInt(1000);
				return variable + " . FILTER(" + variable + "=" + newObject + ") ";
			} else {
				// It is a language annotated string
				return object;
			}
		}
	}

	private static String getNextVariable(String subjectVariable, int suffixForObjectVariable) {
		Pattern pattern = Pattern.compile("\\?x([0-9]+)");
		Matcher matcher = pattern.matcher(subjectVariable);
		if (matcher.matches()) {
			int id = Integer.parseInt(matcher.group(1)) + suffixForObjectVariable;
			return "?x" + id;
		} else {
			throw new IllegalArgumentException("Invalid variable name " + subjectVariable);
		}

	}

	@Override
	public boolean existsBinding(List<String[]> atoms) {
		if (atoms.isEmpty()) {
			return true;
		}

		// Transform this into a query
		int varPosition = getFirstVariablePosition(atoms.get(0));
		String queryString = HDTKB.atoms2Query(atoms, atoms.get(0)[varPosition]);
		queryString += " LIMIT 1";
		Query query = QueryFactory.create(queryString);
		Model model = getModel();
		try (QueryExecution qexec = QueryExecutionFactory.create(query, model)) {
			ResultSet results = qexec.execSelect();
			if (results.hasNext()) {
				return true;
			}
		}

		return false;
	}

	private int getFirstVariablePosition(String[] strings) {
		for (int i = 0; i < strings.length; ++i) {
			if (strings[i].equals(""))
				return i;
		}

		return -1;
	}

	@Override
	public long countDistinct(String variable, List<String[]> atoms) {
		if (atoms.size() == 1) {
			String[] theAtom = atoms.get(0);
			if (variable.equals(theAtom[2])
					&& theAtom[0].startsWith("?")) {
				return cachedRelationObjectCountCardinalities.get(theAtom[1]);
			}						
		}		
		
		String query = atoms2Query(atoms, variable);
		query = query.replace("SELECT DISTINCT " + variable, "SELECT (count(distinct " + variable + ") as ?cc)");
		Long answer = countDistinctCache.get(query);
		if (answer == null) {
			answer = Long.parseLong(execute(query, "?cc").iterator().next().replace("\"", "")
					.replace("^^http://www.w3.org/2001/XMLSchema#integer", ""));
			countDistinctCache.put(query, answer);
		}
		return answer;
	}

	/**
	 * check if the conditions is a part of cached queries or not, if yes, return the count distinct of it
	 * else return 0
	 * @param atoms
	 * @return
	 */
	private long isInCachedQueries(List<String[]> atoms) {
		if (atoms.size() == 1) {
			String[] query = atoms.get(0);
			String predicate = query[1];
			String object = query[2];
			String key = null;
			if (object.startsWith("?")) {
				key = "?y";
			} else {
				key = object;
			}
			Long value = this.cachedTypeCardinalities.get(key);
			if (GlobalConfig.typeRelation.equals(predicate) && value != null) {
				return value;
			} else {
				return 0;
			}
		} else {
			return 0;
		}
	}
	
	private <V> V getFromCache(String query, Map<Long, LRUQueryCache<V>> cache) {
		V v = null;
		synchronized(cache) {
			Long threadId = Thread.currentThread().getId();
			if (cache.containsKey(threadId)) {
				v = cache.get(threadId).get(query);
			}
		}
		
		return v;
	}

	@Override
	public String cleanString(String str) {
		String result = new String(str);
		for (String key : prefixesMap.keySet()) {
			if (str.contains(key)) {
				result = result.replace(key, prefixesMap.get(key));
			}
		}
		
		return result;
	}

	@Override
	public long countDistinct(String variable1, String variable2, List<String[]> atoms) {
		if (atoms.size() == 1) {
			String[] atom = atoms.get(0);
			if (atom[0].startsWith("?") && atom[2].startsWith("?")
						&& !atom[0].equals(atom[2])) { 
				String relation = atom[1];
				
				if (cachedRelationCardinalities.containsKey(relation)) {
					return cachedRelationCardinalities.get(relation);
				}
			}
		}
		
		String query = atoms2Query(atoms, variable2);
		query = query.replace("SELECT DISTINCT " + variable2, "SELECT " + variable1 +  " (count(distinct " + variable2 + ") as ?co)");
		query += " GROUP BY " + variable1;
		query = "SELECT (sum(?co) as ?cc) WHERE { " + query + "}";
		//System.out.println(query);
		Long answer = countDistinctCache.get(query);
		if (answer == null) {
			answer = Long.parseLong(execute(query, "?cc").iterator().next().replace("\"", "")
						.replace("^^http://www.w3.org/2001/XMLSchema#integer", ""));
			countDistinctCache.put(query, answer);	
		}
		//System.out.println("Done, answer=" + answer + ", " + (System.currentTimeMillis() - a) + "ms");
		return answer;
	}

	@Override
	public long size() {
		return data.size();
	}
	
	public static void main(String[] args) throws IOException, InterruptedException {
		GlobalConfig.setKbPath("src/data/dbpedia/", "dbpedia.hdt");
		HDTKB kb = new HDTKB(GlobalConfig.getKbPath());
		System.out.println(
				kb.execute("SELECT DISTINCT ?x0 WHERE { ?x0 <http://dbpedia.org/ontology/internationally> ?o . filter(?o=\"true\"^^<http://www.w3.org/2001/XMLSchema#boolean>)  ?x0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://dbpedia.org/ontology/Company> . }", 
						"?x0"));
//		System.out.println(wrapEntity("\"true\"^^http://www.w3.org/2001/XMLSchema#boolean"));
		
//		System.out.println(kb.execute(
//				"SELECT DISTINCT ?x0 WHERE { ?x0 <http://dbpedia.org/ontology/product> ?x1  . ?x1 <http://dbpedia.org/ontology/license> <http://dbpedia.org/resource/Proprietary_software>  . }", "?x0"));

		System.out.println(kb.execute(
				"SELECT DISTINCT ?x0 WHERE { ?x0 <http://dbpedia.org/ontology/mayor> ?x1  . ?x1 <http://dbpedia.org/ontology/activeYearsStartDate> ?z FILTER(?z=\"2014-04-05\"^^<http://www.w3.org/2001/XMLSchema#date>) . }", "?x0"));
		
		System.out.println(kb.execute(
				"SELECT DISTINCT ?x0 WHERE { ?x0 <http://dbpedia.org/ontology/equity> ?xy803  . FILTER(?xy803=\"1.04014193E8\"^^<http://dbpedia.org/datatype/euro>) . }", "?x0"));

		System.out.println(kb.execute(
				"SELECT DISTINCT ?x0 WHERE { ?x0 <http://dbpedia.org/ontology/populationTotal> ?xy220 . FILTER(?xy220=\"2229621\"^^<http://www.w3.org/2001/XMLSchema#nonNegativeInteger>) . }", "?x0"));

		
	}

	@Override
	public double countDistinctP() {
		return numberOfPredicates;
	}


}
