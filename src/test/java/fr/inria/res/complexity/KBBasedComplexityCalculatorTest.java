package fr.inria.res.complexity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

import fr.inria.res.Atom;
import fr.inria.res.Condition;
import fr.inria.res.data.HDTKB;
import fr.inria.res.data.InMemoryKB;
import fr.inria.res.data.KB;

class KBBasedComplexityCalculatorTest {
	static KB kb1;
	static KB kb2;
	static KB kb3;
	static double cConstant = KBBasedComplexityCalculator.complexityConstant;
	
	static {
		kb1 = new InMemoryKB();
		InMemoryKB ikb1 =(InMemoryKB)kb1;
		ikb1.add("<Luis>", "<livesIn>", "<Ecuador>");
		ikb1.add("<Ecuador>", "<officialLang>", "<Spanish>");
		
		ikb1.add("<Pedro>", "<livesIn>", "<Peru>");
		ikb1.add("<Peru>", "<officialLang>", "<Spanish>");
		
		ikb1.add("<Spanish>", "<langFamily>", "<Romance>");		
		ikb1.add("<Portuguese>", "<langFamily>", "<Romance>");
		ikb1.add("<French>", "<langFamily>", "<Romance>");		
		
		kb2 = new InMemoryKB();
		InMemoryKB ikb2 =(InMemoryKB)kb2;
		try {
			ikb2.load(new File("src/data/test/kb.rdf"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			kb3 = new HDTKB("src/data/test/test.hdt");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	void testSimpleConditionInDiskKB() {
		double expectedCardinality = 2.0;
		KBBasedComplexityCalculator cc = new KBBasedComplexityCalculator(kb3);
		Condition condition = Condition.makeCondition("http://example.org/graduatedFrom", "http://example.org/INSA");
		double expectedComplexity = cConstant - Math.log1p(expectedCardinality) / Math.log(2);
		assertEquals(expectedComplexity, cc.getComplexity(condition), 1e-8);
	}
	
	@Test
	void testLongerPathInDiskKB() {
		Condition condition = Condition.makeCondition("http://example.org/worksAt", "http://example.org/Energiency");
		Atom newAtom1 = new Atom("http://example.org/Energiency", "http://example.org/type", "http://example.org/Company", null);
		Atom newAtom2 = new Atom("http://example.org/Energiency", "http://example.org/type", "http://example.org/Startup", null);
		Condition pathCondition = condition.addAtoms(Arrays.asList(newAtom1, newAtom2), 
				condition.getSetAtoms().iterator().next());

		System.out.println(pathCondition);
		
		KBBasedComplexityCalculator cc = new KBBasedComplexityCalculator(kb3);
		double complexity = cc.getComplexity(pathCondition);
		double expectedCardinality = 21;
		// 2 times 5 subjects for worksAt(x, y) + 2 times 4 subjects for is(x, y) worksAt(z, x) + 
		// 2 subjects for is(x, Company) worksAt(z, x) and 1 subject for is(x, Startup) worksAt(z, x)
		double expectedComplexity = cConstant - Math.log1p(expectedCardinality) / Math.log(2);
		assertEquals(expectedComplexity, complexity, 1e-8);		
	}
	

	@Test
	void testSimpleCondition() {
		KBBasedComplexityCalculator cc = new KBBasedComplexityCalculator(kb1);
		Condition condition = Condition.makeCondition("<livesIn>", "<Ecuador>");
		// The cumulative cardinality of this atom is 3 because
		// the cardinality of livesIn(x, Ecuador) = 1 and the cardinality of livesIn(x, y) = 2
		double expectedComplexity = cConstant - Math.log1p(3.0) / Math.log(2);
		// We are comparing real numbers so we need a delta
		assertEquals(expectedComplexity, cc.getComplexity(condition), 1e-8);
	}
	
	
	@Test
	void testSimpleCondition1() {
		double expectedCardinality = 2.0;
		KBBasedComplexityCalculator cc = new KBBasedComplexityCalculator(kb2);
		Condition condition = Condition.makeCondition("<graduatedFrom>", "<INSA>");
		double expectedComplexity = cConstant - Math.log1p(expectedCardinality) / Math.log(2);
		assertEquals(expectedComplexity, cc.getComplexity(condition), 1e-8);

	}
	
	@Test
	void testPathCondition() {
		Condition condition = Condition.makeCondition("<worksAt>", "<IRISA>");
		Atom newAtom = new Atom("<IRISA>", "<headquarteredIn>", "<Rennes>", null);
		Condition pathCondition = condition.chainAtomSO(newAtom, condition.getSetAtoms().iterator().next());
		System.out.println(pathCondition);
		
		KBBasedComplexityCalculator cc = new KBBasedComplexityCalculator(kb2);
		double complexity = cc.getComplexity(pathCondition);
		double expectedCardinality = 8;
		// 5 subjects for worksAt(x, y) + 2 subjects for headquarteredIn(x, y) worksAt(z, y) + 
		// 1 subjects for headquarteredIn(x, Rennes) worksAt(z, x)
		double expectedComplexity = cConstant - Math.log1p(expectedCardinality) / Math.log(2);
		assertEquals(expectedComplexity, complexity, 1e-8);		
	}
	
	@Test
	void testStarCondition() {
		Condition condition = Condition.makeCondition("<worksAt>", "<Energiency>");
		Atom newAtom1 = new Atom("<Energiency>", "<rdf:type>", "<Company>", null);
		Atom newAtom2 = new Atom("<Energiency>", "<rdf:type>", "<Startup>", null);
		Condition starCondition = condition.addAtom(newAtom1, newAtom2, 
				condition.getSetAtoms().iterator().next());

		System.out.println(starCondition);
		
		KBBasedComplexityCalculator cc = new KBBasedComplexityCalculator(kb2);
		double complexity = cc.getComplexity(starCondition);
		double expectedCardinality = 16;
		// 5 subjects for worksAt(x, y) + 2 times 4 subjects for is(x, y) worksAt(z, x) + 
		// 2 subjects for is(x, Company) worksAt(z, x) and 1 subject for is(x, Startup) worksAt(z, x)
		double expectedComplexity = cConstant - Math.log1p(expectedCardinality) / Math.log(2);
		assertEquals(expectedComplexity, complexity, 1e-8);		
	}
	
	@Test
	void testLongerPath() {
		Condition condition = Condition.makeCondition("<worksAt>", "<Energiency>");
		Atom newAtom1 = new Atom("<Energiency>", "<rdf:type>", "<Company>", null);
		Atom newAtom2 = new Atom("<Energiency>", "<rdf:type>", "<Startup>", null);
		Condition pathCondition = condition.addAtoms(Arrays.asList(newAtom1, newAtom2), 
				condition.getSetAtoms().iterator().next());

		System.out.println(pathCondition);
		
		KBBasedComplexityCalculator cc = new KBBasedComplexityCalculator(kb2);
		double complexity = cc.getComplexity(pathCondition);
		double expectedCardinality = 21;
		// 2 times 5 subjects for worksAt(x, y) + 2 times 4 subjects for is(x, y) worksAt(z, x) + 
		// 2 subjects for is(x, Company) worksAt(z, x) and 1 subject for is(x, Startup) worksAt(z, x)
		double expectedComplexity = cConstant - Math.log1p(expectedCardinality) / Math.log(2);
		assertEquals(expectedComplexity, complexity, 1e-8);		
	}
	
	@Test
	void testComplexityComparison() {
		InMemoryKB kb = new InMemoryKB();
		kb.add("<Julien>", "<citizenOf>", "<France>");
		kb.add("<Luis>", "<citizenOf>", "<Ecuador>");
		kb.add("<Gregory>", "<citizenOf>", "<France>");
		kb.add("<Yann>", "<citizenOf>", "<France>");
		
		kb.add("<Julien>", "<profession>", "<Intern>");
		kb.add("<Luis>", "<profession>", "<Researcher>");
		kb.add("<Gregory>", "<profession>", "<GhostIntern>");
		kb.add("<Yann>", "<profession>", "<PhDStudent>");
		
		kb.add("<Julien>", "<livesIn>", "<Rennes>");
		kb.add("<Luis>", "<livesIn>", "<Rennes>");
		kb.add("<Gregory>", "<livesIn>", "<Rennes>");
		kb.add("<Yann>", "<livesIn>", "<Rennes>");
	
		kb.add("<Julien>", "<likes>", "<Cooking>");
		kb.add("<Luis>", "<likes>", "<Travel>");
		kb.add("<Luis>", "<likes>", "<Cooking>");
		kb.add("<Gregory>", "<likes>", "<Cooking>");
		kb.add("<Yann>", "<likes>", "<Read>");
		kb.add("<Yann>", "<likes>", "<Cooking>");
		
		kb.add("<France>", "<hasCapital>", "<Paris>");
		kb.add("<France>", "<likes>", "<Wine>");
		kb.add("<France>", "<citizenAre>", "<French>");
		
		List<String> entities2Describe = new ArrayList<>();
		entities2Describe.add("<Gregory>");
		entities2Describe.add("<Julien>");
		entities2Describe.add("<Yann>");
		Condition cond = Condition.makeCondition("<citizenOf>", "<France>");
		Condition cond1 = cond.addAtoms(Arrays.asList(
				new Atom("<France>", "<hasCapital>", "<Paris>", null),
				new Atom("<France>", "<likes>", "<Wine>", null)), 
				cond.getSetAtoms().iterator().next());
		KBBasedImprovedComplexityCalculator cc = new KBBasedImprovedComplexityCalculator(kb);
		double cx1 = cc.getComplexity(cond1);
		double cx = cc.getComplexity(cond);
		System.out.println(cond + " Complexity: " + cx);
		System.out.println(cond1 + " Complexity: " + cx1);		
		assertTrue(cx1 > cx);
	}

}
